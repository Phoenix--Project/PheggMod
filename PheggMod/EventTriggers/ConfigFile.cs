﻿#pragma warning disable CS0626 // orig_ method is marked external and has no attributes on it.
using Dissonance.Extensions;
using MonoMod;
using System;
using System.Linq;

namespace PheggMod.EventTriggers
{
    [MonoModPatch("global::GameCore.ConfigFile")]
    public static class PMConfigFile
    {
        internal static string webhookUrl, webhookName, webhookAvatar, webhookMessage, reportHeader, reportContent, reportServerName;
        internal static int webhookColour, detonationTimer;

        internal static bool announceChaos, cassieGlitch, cassieGlitchDetonation, stickyRound, targetAnnouncement, mockCommand, tutorialTrigger096, enable008;
        internal static string chaosAnnouncement;

        internal static float doorCooldown173;

        #region SmartGuard;
        internal static bool enableSmartGuard, skipServerStaff, skipGlobalStaff, skipStudioStaff, smartFilter, defaultProfile, profNotSet;
        internal static string[] whitelistedNames, whitelistedUids, blacklistedNames, blacklistedUids;
        internal static int minorInfractionBan, mediumInfractionBan, MajorInfractionBan, accountAgeRequirement;
        internal static string SteamKey;
        #endregion

        public static extern void orig_ReloadGameConfigs(bool firstTime = false);
        public static void ReloadGameConfigs(bool firstTime = false)
        {
            orig_ReloadGameConfigs(firstTime);

            YamlConfig ServerConfig = GameCore.ConfigFile.ServerConfig;

            announceChaos = ServerConfig.GetBool("announce_chaos_spawn", true);
            chaosAnnouncement = ServerConfig.GetString("chaos_announcement", "PITCH_1 ATTENTION ALL PERSONNEL . CHAOS INSURGENCY BREACH IN PROGRESS");
            cassieGlitch = ServerConfig.GetBool("cassie_glitch", false);
            cassieGlitchDetonation = ServerConfig.GetBool("cassie_glitch_post_detonation", false);
            stickyRound = ServerConfig.GetBool("fix_sticky_round", true);
            mockCommand = ServerConfig.GetBool("enable_mock_command", true);
            targetAnnouncement = ServerConfig.GetBool("notify_096_target", true);
            tutorialTrigger096 = ServerConfig.GetBool("tutorial_triggers_096", false);
            enable008 = ServerConfig.GetBool("scp_008", true);
            detonationTimer = ServerConfig.GetInt("warhead_tminus_start_duration", 90);

            webhookUrl = ServerConfig.GetString("report_discord_webhook_url", string.Empty);
            webhookName = ServerConfig.GetString("report_username", "Player Report");
            reportServerName = ServerConfig.GetString("report_server_name", "My SCP:SL Server");
            reportHeader = ServerConfig.GetString("report_header", "Player Report");
            reportContent = ServerConfig.GetString("report_content", "Player has just been reported.");
            webhookAvatar = ServerConfig.GetString("report_avatar_url", string.Empty);
            webhookMessage = ServerConfig.GetString("report_message_content", string.Empty);
            webhookColour = ServerConfig.GetInt("report_color", 14423100);

            doorCooldown173 = ServerConfig.GetFloat("scp173_door_cooldown", 25f);

            #region SmartGuard
            enableSmartGuard = ServerConfig.GetBool("sg_enabled", true);
            skipServerStaff = ServerConfig.GetBool("sg_skip_staff_server", true);
            skipGlobalStaff = ServerConfig.GetBool("sg_skip_global_staff", true);
            skipGlobalStaff = ServerConfig.GetBool("sg_skip_studio_staff", true);
            smartFilter = ServerConfig.GetBool("sg_smart_filter", true);

            whitelistedNames = ServerConfig.GetStringList("sg_whitelist_names").ToArray();
            blacklistedNames = ServerConfig.GetStringList("sg_blacklist_names").ToArray();
            blacklistedNames.Concat(new string[] { "kite1101", "beefteef420", "saltcollector" }).ToArray();

            blacklistedUids = ServerConfig.GetStringList("sg_whitelist_uids").ToArray();
            whitelistedUids = ServerConfig.GetStringList("sg_whitelist_uids").ToArray();

            minorInfractionBan = ServerConfig.GetInt("sg_minor_duration", 15); //15 minutes
            mediumInfractionBan = ServerConfig.GetInt("sg_medium_duration", 4320); //3 days
            MajorInfractionBan = ServerConfig.GetInt("sg_major_duration", 20160); //2 weeks

            accountAgeRequirement = ServerConfig.GetInt("sg_minimum_age", 10080);

            SteamKey = ServerConfig.GetString("steam_api_key", null);
            defaultProfile = ServerConfig.GetBool("sg_steam_pdefaultprofile", false);
            profNotSet = ServerConfig.GetBool("sg_steam_pprofilenotset", true);
            #endregion
        }
    }
}
